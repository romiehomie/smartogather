FROM fpco/stack-build as builder
RUN mkdir /opt/build
COPY . /opt/build
RUN cd /opt/build && stack install --system-ghc

FROM ubuntu:20.04
RUN apt-get update && apt-get install -y \
  ca-certificates \
  libgmp-dev
RUN mkdir -p /opt/bin/voicechat/
RUN mkdir -p /opt/bin/voicechat/assets
WORKDIR /opt/bin/voicechat
COPY --from=builder /root/.local/bin/ws-voicechat-exe .
COPY --from=builder /opt/build/assets assets
CMD ["/opt/bin/voicechat/ws-voicechat-exe"]
