module Application (runApp) where

import Control.Concurrent (MVar, newMVar, readMVar, withMVar, modifyMVar_)
import Control.Exception (finally)
import Control.Monad (forM_, forever)

import Data.Aeson
import Data.Char (isSpace)
import qualified Data.Text as T
import qualified Data.Text.Encoding as E

import System.Environment (lookupEnv)

import Network.Wai 
import Network.Wai.Application.Static (staticApp, defaultFileServerSettings, ssGetMimeType)
import Network.Wai.Handler.Warp (run)
import Network.Wai.Handler.WebSockets (websocketsOr)
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import qualified Network.WebSockets as WS

import WaiAppStatic.Types (fileName, fromPiece)

data Client = Client {
  cUid :: T.Text, 
  cConn :: WS.Connection
}

data ClientMsg = ClientHello T.Text

instance FromJSON ClientMsg where
  parseJSON = withObject "ProtocolMsg" $ \v -> do
    msgType <- v .: "type"
    case msgType of
      "ClientHello" -> ClientHello <$> v .: "uid"
      _ -> fail $ "unknown client msg type: " ++ msgType

data ServerMsg = 
  ServerError T.Text | ServerHello T.Text [T.Text] | PeerJoined T.Text | PeerLeft T.Text

instance ToJSON ServerMsg where
  toJSON (ServerError txt) = object ["type" .= ("ServerError"::T.Text), "error" .= txt]
  toJSON (PeerJoined uid) = object ["type" .= ("PeerJoined"::T.Text), "uid" .= uid]
  toJSON (PeerLeft uid) = object ["type" .= ("PeerLeft"::T.Text), "uid" .= uid]
  toJSON (ServerHello uid peers) = object ["type" .= ("ServerHello"::T.Text), "uid" .= uid, "peers" .= peers]

writeToConn :: WS.Connection -> ServerMsg -> IO ()
writeToConn conn msg = WS.sendTextData conn $ encode msg

receiveFromConn :: WS.Connection -> IO (Either T.Text ClientMsg)
receiveFromConn conn = do
    msg <- WS.receiveData conn
    case eitherDecode msg of
      Left err -> return $ Left (T.pack err)
      Right (ClientHello name) -> return $ Right (ClientHello name)

invalidUserNameMsg :: T.Text -> ServerMsg
invalidUserNameMsg err = ServerError $ "Bad Username: " <> err

userAlreadyExistsMsg :: T.Text -> ServerMsg
userAlreadyExistsMsg err = ServerError $ "User Already Exists: " <> err

badAnnouncementMsg :: T.Text -> ServerMsg
badAnnouncementMsg err = ServerError $ "Bad Announcement: " <> err

otherErrorMsg :: T.Text -> ServerMsg
otherErrorMsg err = ServerError $ "Other Error:" <> err

peerJoinedMsg :: T.Text -> ServerMsg
peerJoinedMsg uid = PeerJoined uid

peerLeftMsg :: T.Text -> ServerMsg
peerLeftMsg uid = PeerLeft uid

serverHelloMsg :: T.Text -> [T.Text] -> ServerMsg
serverHelloMsg uid peers = ServerHello uid peers

newClient :: T.Text -> WS.Connection -> Client
newClient name conn = Client { cUid = name, cConn = conn }

data ServerState = ServerState {
  ssClients :: [Client]
}

emptyServerState :: ServerState
emptyServerState = ServerState { ssClients = [] }

clientExists :: Client -> ServerState -> Bool
clientExists client (ServerState clients) = let name = cUid client in any ((== name) . cUid) clients

addClient :: Client -> ServerState -> ServerState
addClient client (ServerState clients) = ServerState $ client:clients

removeClient :: Client -> ServerState -> ServerState
removeClient client (ServerState clients) = 
  let clients' = filter ((/= cUid client) . cUid) clients in ServerState clients'

runApp :: IO ()
runApp = do 
  serverState <- newMVar emptyServerState
  port <- portFromEnvOrDefault
  run port $ logStdoutDev $ app serverState
  where
    portFromEnvOrDefault = do 
      mbPort <- lookupEnv "PORT"
      case mbPort of
        Just portStr -> return (read portStr :: Int)
        Nothing -> return 80

app :: MVar ServerState -> Application
app serverState = websocketsOr WS.defaultConnectionOptions (wsApp serverState) webApp

webApp :: Application
webApp = staticApp $ staticSettings { ssGetMimeType = getMimeType }
  where
    staticSettings = defaultFileServerSettings "assets" 
    getMimeType f = return $ mimeTypeForName . fromPiece . fileName $ f
    mimeTypeForName name
      | T.isSuffixOf ".js" name = "text/javascript"
      | T.isSuffixOf ".html" name = "text/html"
      | otherwise = "application/octet-stream"

wsApp :: MVar ServerState -> WS.ServerApp
wsApp state pconn = wsDispatch state path pconn
  where
    path = E.decodeUtf8 . WS.requestPath . WS.pendingRequest $ pconn

wsDispatch :: MVar ServerState -> T.Text -> WS.PendingConnection -> IO ()
wsDispatch state path pconn = case path of
  "/ws" -> wsHandleWS state pconn
  _ -> WS.rejectRequestWith pconn notFoundRequest 
  where 
    notFoundRequest = (WS.defaultRejectRequest {WS.rejectCode = 404, WS.rejectMessage = "NotFound"})

wsHandleWS :: MVar ServerState -> WS.PendingConnection -> IO ()
wsHandleWS state pconn = do
  conn <- WS.acceptRequest pconn
  WS.withPingThread conn wsPingIntervalSeconds (return ()) $ do
    eitherMsg <- receiveFromConn conn
    case eitherMsg of
      Left err -> writeToConn conn $ badAnnouncementMsg err
      Right (ClientHello name)-> do
        let client = newClient name conn
        alreadyExists <- clientExists' client
        let badName = any ($ name) [T.null, T.any isSpace]
        case name of
          _  | badName -> writeToConn conn $ invalidUserNameMsg name
             | alreadyExists -> writeToConn conn $ userAlreadyExistsMsg name
             | otherwise -> joinRoom client state
  where
    wsPingIntervalSeconds = 30
    clientExists' c = withMVar state $ \s -> pure $ clientExists c s

joinRoom :: Client -> MVar ServerState -> IO ()
joinRoom client state = do 
  connect client
  flip finally (disconnect client) $ do 
    forever $ do
      eitherMsg <- receiveFromConn (cConn client)
      case eitherMsg of 
        Left err -> writeToConn (cConn client) $ otherErrorMsg err
        Right _ -> writeToConn (cConn client) $ otherErrorMsg "did not expect message"
  where
    connect c = do 
      modifyMVar_ state $ \s -> pure $ addClient c s
      readMVar state >>= \s -> do 
        broadcast client (peerJoinedMsg $ cUid client) s
        let peers = cUid <$> [cc | cc <- (ssClients s), cUid c /= cUid cc]
        writeToConn (cConn c) (serverHelloMsg (cUid c) peers) 
    disconnect c = do 
      modifyMVar_ state $ \s -> pure $ removeClient c s
      readMVar state >>= broadcast client (peerLeftMsg $ cUid client)

broadcast :: Client -> ServerMsg -> ServerState -> IO ()
broadcast cli msg serverState = forM_ clients $ \c -> writeToConn (cConn c) msg
  where clients = [c | c <- ssClients serverState, cUid cli /= cUid c]
