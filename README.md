# smartOgather

Browser based peer to peer video chat program haskell and javascript.
When the basic features are well implemented, avatar chat features will become the next focus.

## Structure

The project root directory contains several sub directories
* /app: contains Main.hs which runs /src/Application.hs
* /assets: contains any non hashcell code i.e. html and js, open index.html to run the application
* /etc: contains RSA key related files
* /src: contains Application.hs ... thats where the magic happens
* /test: test related files
* /.git: git related files - will be created when cloning from gitlab
* /.stack-work: stack related files - will be created when bulding with stack

The project root directory furthermore contains some files
* CHANGELOG.md
* Dockerfile
* LICENSE
* package.yaml 
* README.md: this file you are reading now
* Setup.hs: the low level cabal interface is implemented using this script
* stack.yaml
* ws-voicechat.cabal
* .gitignore


## Build

Linux: You need the haskell stack toolchain. Information on how to install stack should be found on https://docs.haskellstack.org. When stack is on your system, open a terminal, navigate to your "smartogether" location and run "stack build"
